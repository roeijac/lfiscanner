import urllib2, threading, Queue
import argparse
from sys import stdout


class MyThread(threading.Thread):

    def __init__(self,q,tid):
        threading.Thread.__init__(self)
        self.q = q
        self.tid = tid

    # Function to run
    def run(self):
        print '[$] Thread No. ' + str(self.tid) + ' is running...'
        while True:
            f = None
            try:
                f = self.q.get(timeout=1)
            except Queue.Empty:
                return

            # We've got a file
            try:
                # URLLIB2 OPEN EACH FILE
                if args.debugMode:  # Is true..
                    print '[*] Trying to get ' + f
                try:
                    res = urllib2.urlopen(args.url + f)
                except:
                    #print '[$] Got Error: ' + str(res.getcode()) + ' For File: ' + f[:-1]
                    continue
                if res.getcode() == 200 or len(res.read()) > 0:
                    #if f[-1] == '\n':
                    #    print '[!] Found ' + f[:-1] # Without \n
                    #else:
                     #   print '[!] Found ' + f # Without \n
                    if args.outputFile is not None:
                        args.outputFile.write(f)
                    #if args.debugMode:
                        #print res.read()
                else:
                    if args.debugMode:
                        print '[*] File ' + f + ' returned ' + str(res.getcode()) + ' but not found'

                # Calculate Time
                stdout.write('\r' + str(self.q.qsize()) + ' Left')
            except KeyboardInterrupt: # EXIT
                backup = open('BACKUP-'+args.url,'w+')
                try:
                    print 'Backing up..'
                    item = self.q.get(timeout=1)
                    backup.write(item)
                except Queue.Empty:
                    print '[*] Current State Saved'
                    exit(0)
            except:
                raise

            self.q.task_done()


def parse():
    #global url,numOfThreads,depthIndex,debugMode,outputFile,bfFile
    #url, numOfThreads, depthIndex, debugMode, outputFile, bfFile = None
    #outputFile = None
    parser = argparse.ArgumentParser()
    parser.add_argument("-u",help="URL to scan, each file will be appended at the end", action='store', dest='url')
    parser.add_argument("-o",help="Output File", action='store',
                        dest='outputFile')
    parser.add_argument("-t",help="Number of Threads", action='store',
                        dest='numOfThreads')
    parser.add_argument("--debug",help="Debugging Mode", action='store_true', default=False,
                        dest='debugMode')
    parser.add_argument("-d",help="Depth Ident", action='store', default=0,
                        dest='depthIndex')
#   parser.add_argument("p",help="Run Brute Force (Permutations) on the file chosen, without scanning",
    #                   action='store', dest='bfFile')
    global args
    args = parser.parse_args()

    if args.outputFile is not None:
        args.outputFile = open(args.outputFile,'w+')


def init():
    print 'Busy Box above Embedded Linux Systems Scanner'
    print 'Created by:\tRoei Jacobovich'
    print 'Use with caution and for ethical purposes only!'
    parse()


def putWithDepth(file,q):
    # Create depth and push to queue
    if depthString[-1] == '/' and file[0] == '/':
            q.put(depthString + file[1:]) # Remove the mutual slash


def main():
    # Queue of files
    init()

    # Define Queue
    # TODO: Add base wordlist for the queue
    # When file is found, the specific thread is adding this to the main queue
    # One of the threads that are online will grab it
    q = Queue.Queue()
    # TODO OPEN FILE AND PUT EVERYTHING IN Q
    wordlist = open('kaliList2','r')
    global depthString
    depthString = ''
    for index in range(0,int(args.depthIndex)): # Create depth
        depthString += '../'
    for index in wordlist:
        putWithDepth(index, q)

    # Define Extensions
    #global extensions
    #extensions = ['js','html','','php','o','log','htm','c']

    # Run Threads
    threadCollection = []
    for i in range(0, int(args.numOfThreads)):
        thread = MyThread(q, i)
        thread.setDaemon(True)
        thread.start()
        threadCollection.append(thread)
    print '[*] ' + args.numOfThreads + ' Threads Created!'
    stdout.write('FINISHED ' + '0%')
    # Wait for everyone to finish
    q.join()
    global counter
    counter = 0
    for item in threadCollection:
        item.join()

    print '[*] Finished!'

    wordlist.close()
    if args.outputFile is not None:
        args.outputFile.close()

    exit(0)


if __name__ == '__main__':
    main()
